#!/bin/bash

# checkout config repo. 
# add credentials on build, Certificate environment variable SSH_PRIVATE_KEY is Base64 encoded!
mkdir /root/.ssh/
echo "${SSH_PRIVATE_KEY}" | base64 --decode > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa

# make sure your domain is accepted
touch /root/.ssh/known_hosts
ssh-keyscan gitlab.com >> /root/.ssh/known_hosts

# Create directory structure
mkdir -p /medialibrary-share/${STREETNAME}/harvest
mkdir -p /medialibrary-share/${STREETNAME}/duplicates
mkdir -p /medialibrary-share/${STREETNAME}/resubmit
mkdir -p /medialibrary-share/${STREETNAME}/production
mkdir -p /medialibrary-share/staging/${STREETNAME}
mkdir -p /medialibrary-share/${STREETNAME}/log
mkdir -p /medialibrary-share/${STREETNAME}/errors


# echo checkout repo but empty directory first so clone will not fail
if [ ! -d /etc/medialibrary/.git ]
then
  rm -rf /etc/medialibrary/*
fi

# clone or pull
git clone git@gitlab.com:naturalis/bii/medialib/publisher-configs.git /etc/medialibrary 2> /dev/null || git -C /etc/medialibrary reset --hard && git -C /etc/medialibrary pull

# Configure config.ini, Replace variables inside the script /opt/medialibrary/config.ini

function updateSetEnv() {
  local propertyname=${1//_/.}
  local propertyvalue=$2
  /bin/sed -i -E "s/${propertyname}.*/${propertyname}\ =\ ${propertyvalue}/" /etc/medialibrary/config-${STREETNAME}.ini
}

function setAllSetEnvs() {
  local env_vars=$(env | awk -F= '/^SETCONF/ {print $1}')
  for env_variable in $env_vars
  do
    local propertyName=${env_variable#"SETCONF_"}
    updateSetEnv "$propertyName" "${!env_variable}"
  done
}

setAllSetEnvs

# keep container alive.. probably bit nasty 
tail -f /dev/null
