FROM php:7.4.3-cli
MAINTAINER hugo.vanduijn@naturalis.nl
ARG PHP_MAX_EXECUTION_TIME=60
ARG PHP_MAX_INPUT_TIME=120
ARG PHP_POST_MAX_SIZE=32M
ARG PHP_UPLOAD_MAX_FILESIZE=32M
ARG PHP_MEMORY_LIMIT=512M

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get -y upgrade \
    && DEBIAN_FRONTEND=noninteractive apt-get -y install apt-utils \
        libcurl4-openssl-dev \
        libpq-dev \
        libpng-dev \
        libjpeg-dev \
        libfreetype6-dev \
        mariadb-client \
        git \
        msmtp \
        imagemagick \
        zip \
        unzip \
    && rm -rf /var/lib/apt/lists/*


RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install \
    gd \
    pdo_mysql

# add msmtprc config for sending emails using php
ADD ./msmtprc /etc/msmtprc

# Download medialibrary publisher code and copy config template to ini. 
RUN git clone https://gitlab.com/naturalis/bii/medialib/medialibrary-publisher.git /opt/medialibrary \
    && cp /opt/medialibrary/config.ini.tpl /opt/medialibrary/config.ini

# Install composer and install vendor modules
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && \
    chmod +x /usr/bin/composer && \
    cd /opt/medialibrary && \
    composer install

# Use the default production configuration for php
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN /bin/sed -i -E "s/max_execution_time = .*/max_execution_time = $PHP_MAX_EXECUTION_TIME/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/max_input_time = .*/max_input_time = $PHP_MAX_INPUT_TIME/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/post_max_size = .*/post_max_size = $PHP_POST_MAX_SIZE/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/upload_max_filesize = .*/upload_max_filesize = $PHP_UPLOAD_MAX_FILESIZE/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/memory_limit = .*/memory_limit = $PHP_MEMORY_LIMIT/" "$PHP_INI_DIR/php.ini" && \
    /bin/sed -i -E "s/;sendmail_path =.*/sendmail_path = \/usr\/bin\/msmtp -t/" "$PHP_INI_DIR/php.ini"

ADD ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

# Serve the application using entrypoint so persistent config is possible
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]